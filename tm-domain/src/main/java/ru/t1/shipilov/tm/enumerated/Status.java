package ru.t1.shipilov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Status status : values()) {
            if (status.getDisplayName().equals(value)) return status;
        }
        return null;
    }

    @NotNull
    public static String[] getDisplayNames() {
        @NotNull String[] statusValues = new String[Status.values().length];
        for (@NotNull final Status status : Status.values()) {
            statusValues[status.ordinal()] = status.displayName;
        }
        return statusValues;
    }

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
